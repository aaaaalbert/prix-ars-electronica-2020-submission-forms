# Computer Animation

## Project

* Title of the project*
* Description of the project* (max. 2000 characters including spaces)
* Type of the project* (Animation, Expanded Animation, Movie, Visual Effect, VR, Other)
* Year the project was created* (2020, 2019, 2018)
* Duration of the full work entered* (Note: HH:MM:SS)
* URL
* Username/Password (Note: If your project is available in the internet,
  please provide the URL(s) that can be used to access your project.
  If the access to your project needs a login, please also provide a
  username and password.)
* Credits
* Support received from
* Keywords
* Software (Note: Which software is used for your project?)
* Hardware (Note: Which kind of hardware was used for your project?)
* [ ] Do you want to submit your project additionally to prize
  consideration for the STARTS Prize?

