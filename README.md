# Prix Ars Electronica 2020 Submission Forms

The files in this repository mirror the submission forms for
[Prix Ars Electronica 2020](https://ars.electronica.art/prix/).
These forms are available from the [online submission system](https://calls.ars.electronica.art/prix/)
after login.

See also the submission FAQs in [German](https://ars.electronica.art/prix/de/faq/)
and [English](https://ars.electronica.art/prix/en/faq/).

Deadline for submissions in the online system: **March 02, 2020.**

# Disclaimer

*Please note:* This is a mere *fan project* which attempts to make the
information about requirements for Prix Ars Electronica submission
more widely accessible.

This repo is not endorsed nor its author affiliated with Ars Electronica.
The information mirrored here is **(c) 2020 Ars Electronica Linz GmbH & Co KG**.

-----

# Structure of submissions

* Before being able to submit, users must register and enter data
  for their [account](account.md).
* Once logged in, users may add projects in different categories and
  fill in the associated forms:
  - [Interactive Art +](interactiveart.md)
  - [Digital Communities](digitalcommunities.md)
  - [Computer Animation](computeranimation.md)
* For every category, artist information may be added:
  - [Artist](artists.md#artist)
  - [Artist group](artists.md#artistgroup)
