# Digital Communities

## Project

* Title of the project*
* Description of the project* (max. 2000 characters including spaces)
* Type of the project* (Community Project, Publication, Social Software, Other)
* Year the project was created* (2020 down to 2000)
* URL
* Username/Password (Note: If your project is available in the internet,
  please provide the URL(s) that can be used to access your project.
  If the access to your project needs a login, please also provide a
  username and password.)
* Credits
* Support received from
* Objectives (Note: What is the objective of your project? What is the
  common goal, topic, interest, etc. of the community?)
* Project History (Note: What was the project's origin, when and how
  did it start? How did it develop up to the present day?)
* People (Note: What is the core team carrying the project? How many
  (groups of) indivisuals are currently involved as members or users?
  How would you characterize the people participating in the project?
  Is access to the project open or restricted?)
* Lessons Learned (What has worked / what has not worked in the process
  of realization of your project?) 
* Keywords
* Software (Note: Which software is used for your project?)
* Hardware (Note: Which kind of hardware was used for your project?)
* [ ] Do you want to submit your project additionally to prize
  consideration for the STARTS Prize?
