# Artists

### Artist
* First name*
* Last name*
* Artist name
* Gender* (Female, Male, Other)
* Date of Birth
* Nationality*
* Biography* (Note: 3000 characters including spaces)
* Portrait* (Note: file upload)
* Biography file (Note: file upload)
* Address
* Postal Code
* City
* State
* Country
* Email*
* Phone*
* Url

### Artistgroup
* Name*
* Photo
* Nationality*
* History (Note: max. 3000 characters including spaces
* History (Note: file upload)
* Contact email*
* Contact phone*
* Url

