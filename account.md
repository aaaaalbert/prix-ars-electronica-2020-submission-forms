# Account Settings

(Fields marked with "*" are required.)

- Email address
- First name*
- Last name*
- Gender* (female, male, other)
- Nationality*
- Address*
- Postal code*
- City*
- State
- Country*
- Phonenumber*
- Contact Type (email, youtube, facebook)
- Contact (text)
